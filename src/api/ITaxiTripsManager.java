package api;

import model.data_structures.BinaryHeap;
import model.vo.Compania;
import model.vo.RangoFechaHora;
import model.vo.Taxi;

/**
 * Basic API for testing the functionality of the TaxiTrip manager
 */
public interface ITaxiTripsManager {

	public boolean cargarSistema(String direccionJson,RangoFechaHora r);
	
	public BinaryHeap<Taxi> taxiSort();
	
	public BinaryHeap<Compania>getCompaniesByNumber();
	
}
