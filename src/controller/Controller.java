package controller;

import model.vo.*;
import api.ITaxiTripsManager;
import model.data_structures.BinaryHeap;
import model.logic.TaxiTripsManager;


public class Controller {


	private static ITaxiTripsManager  manager = new TaxiTripsManager();
	
	public static boolean cargarSistema(String direccionJson, RangoFechaHora r)
	{
		return manager.cargarSistema(direccionJson, r);
	}
	public static BinaryHeap<Taxi> taxiSort()
	{
		return manager.taxiSort();
	}
	public static BinaryHeap<Compania> getCompaniesByNumber()
	{
		return manager.getCompaniesByNumber();
	}
}
