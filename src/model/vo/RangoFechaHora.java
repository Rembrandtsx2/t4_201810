package model.vo;

/**
 * Modela una rango de fechas y horas (iniciales y finales)
 *
 */
public class RangoFechaHora
{
	//ATRIBUTOS
	
    /**
     * Modela la fecha inicial del rango
     */
	private String startDate; 
	
	/**
	 * Modela la fecha final del rango
	 */
	private String endDate;
	
	/**
	 * modela la hora inicial del rango
	 */
	private String startHour; 
	
	/**
	 * modela la hora final del rango
	 */
	private String endHour;
	
	//CONSTRUCTOR
	/**
	 * @param pFechaInicial, fecha inicial del rango
	 * @param pFechaFinal, fecha final del rango
	 * @param pHoraInicio, hora inicial del rango
	 * @param pHoraFinal, hora final del rango
	 */
	public RangoFechaHora(String pFechaInicial, String pFechaFinal, String pHoraInicio, String pHoraFinal)
	{
		this.endDate = pFechaFinal;
		this.startDate = pFechaInicial;
		this.endHour = pHoraFinal;
		this.startHour = pHoraInicio;
	}
	public RangoFechaHora(String fecha, String pHoraInicio, String pHoraFinal)
	{
		this.startDate = fecha;
		this.endDate = fecha;
		this.startHour = pHoraInicio;
		this.endHour = pHoraFinal;
	}
	//M�TODOS
	
	/**
	 * @return the fechaInicial
	 */
	public String getStartDate() 
	{
		return startDate;
	}

	/**
	 * @param startDate the fechaInicial to set
	 */
	public void setStartHour(String startDate)
	{
		this.startDate = startDate;
	}
	
	public String getStartDateHour()
	{
		return startDate + 'T' + startHour;
	}
	
	public String getEndDateHour()
	{
		return endDate + 'T' + endHour;
	}

	/**
	 * @return the fechaFinal
	 */
	public String getEndDate() 
	{
		return endDate;
	}

	/**
	 * @param fechaFinal the fechaFinal to set
	 */
	public void setEndDate(String fechaFinal) 
	{
		this.endDate = fechaFinal;
	}

	/**
	 * @return the horaInicio
	 */
	public String getStartHour() 
	{
		return startHour;
	}

	/**
	 * @param horaInicio the horaInicio to set
	 */
	public void setHoraInicio(String horaInicio) 
	{
		this.startHour = horaInicio;
	}

	/**
	 * @return the horaFinal
	 */
	public String getHoraFinal() 
	{
		return endHour;
	}

	/**
	 * @param horaFinal the horaFinal to set
	 */
	public void setHoraFinal(String horaFinal) 
	{
		this.endHour = horaFinal;
	}
}
