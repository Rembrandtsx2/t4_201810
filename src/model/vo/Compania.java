package model.vo;

import java.text.ParseException;

import model.data_structures.BinaryHeap;
import model.data_structures.LinkedList;
import model.data_structures.LinkedSimpleList;

public class Compania implements Comparable<Compania> {

	private String name;

	private BinaryHeap<Taxi> taxis;
	

	private int numberOfTaxis;
	

	private int ammountServices;
	
	private int ammountTaxis;

	public Compania(String company_name) 
	{ 
		name = company_name;
		taxis = new BinaryHeap<Taxi>();
		numberOfTaxis = 0;
		ammountTaxis = 0;
		ammountServices = 0;
	}

	public String getNombre() 
	{
		return name;
	}

	public int getNumTaxisCompania()
	{
		return numberOfTaxis;
	}

	public int getNumServiciosEnRango()
	{
		return ammountServices;
	}

	public BinaryHeap<Taxi> getTaxisCompania() 
	{
		return taxis;
	}

	public Taxi addTaxisCompania(Taxi element)
	{
		Taxi t = taxis.add(element);
		numberOfTaxis++;
		return t;
	}

	public Taxi binarySearchTaxi(String pTaxiId)
	{
		ordenarTaxisCompania(true);
		Taxi tx = null;
		boolean existe = false;

		int inicio = 0;
		int fin = taxis.size() - 1;

		while( inicio <= fin && ! existe)
		{
			int medio = (inicio+fin)/2;
			int res = taxis.get(medio).getTaxiId().compareTo(pTaxiId);
			if(res == 0)
			{
				existe = true;
				tx = taxis.get(medio);
			}
			else if(res > 0)

			{
				fin = medio-1;
			}
			else if(res < 0)
			{
				inicio = medio+1;
			}
		}
		return tx;
	}

	public void ordenarTaxisCompania(boolean ascendente)
	{
		taxis.sort(ascendente);
	}
	
	public Taxi existsTaxi(String pId)
	{
		boolean existe = false;
		Taxi miTaxi = null;
		if(!taxis.isEmpty())
		{
			for(int j=0; j< taxis.size() && !existe; j++)
			{
				if(taxis.get(j).getTaxiId().contains(pId))
				{
					miTaxi = taxis.get(j);
					existe = true;
				}
			}
		}
		return miTaxi;
	}
	
	public void asignarNumServiciosDeLaCompaniaEnRango()
	{
		ammountServices ++;
	}
	public void asignarNumDeTaxisEnRango()
	{
		ammountTaxis++;
	}
	public int getNumTaxisInRange()
	{
		return ammountTaxis;
	}

	@Override
	public int compareTo(Compania o) 
	{
		int compare = ammountServices- o.getNumServiciosEnRango();
		if(compare < 0)
		{
			return -1;
		}
		else if(compare > 0)
		{
			return 1;
		}
		else
		{
			return 0;			
		}
	}

	public String toString()
	{
		return name;
	}
	
}