package model.vo;

import java.text.ParseException;

import model.data_structures.LinkedSimpleList;
import model.data_structures.Queue;

/**
 * Representation of a taxi object
 */
public class Taxi implements Comparable<Taxi>
{
	
	private int servAmmount;
	private double money;
	private double distance;
	private String taxi_id;
	private String company;



	public Taxi(String pTaxi_id, String company_name)
	{
		taxi_id = pTaxi_id;
		company = company_name;
		servAmmount = 0;
		money = 0;
		distance = 0.0;
	}

	/**
	 * @return id - taxi_id
	 */
	public String getTaxiId()
	{
		return  taxi_id;
	}

	/**
	 * @return company
	 */
	public String getCompany()
	{
		return company;
	}
	public void asignarNumeroServiciosEnRango()
	{
		servAmmount ++;
	}
	public void asignarDineroEnRango(double trip_total)
	{
		money += trip_total;
	}
	public void asignarDistanciaEnRango(double pDis)
	{
		distance += pDis;
	}
	public double darPlataGanadaEnRango()
	{
		return money;
	}
	public int darNumeroServiciosEnRango()
	{
		return servAmmount;
	}
	public double darDistanciaEnRango()
	{
		return distance;
	}
	@Override
	public int compareTo(Taxi o) 
	{
		int compare = taxi_id.compareTo(o.taxi_id);
		if(compare < 0)
		{
			return -1;
		}
		else if(compare > 0)
		{
			return 1;
		}
		else
		{
			return 0;			
		}
	}
}
