package model.logic;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.google.gson.JsonArray;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import api.ITaxiTripsManager;
import model.data_structures.BinaryHeap;
import model.data_structures.Queue;
import model.data_structures.Stack;
import model.vo.Compania;
import model.vo.RangoFechaHora;
import model.vo.Servicio;
import model.vo.Taxi;

public class TaxiTripsManager implements ITaxiTripsManager {

		public static final String DIRECCION_SMALL_JSON = "./data/taxi-trips-wrvz-psew-subset-small.json";
		public static final String DIRECCION_MEDIUM_JSON = "./data/taxi-trips-wrvz-psew-subset-medium.json";
		public static final String DIRECCION_LARGE_JSON = "./data/taxi-trips-wrvz-psew-subset-large";

		public static final String COMPANY = "company";

		public static final String TRIP_ID = "trip_id";

		public static final String TAXI_ID ="taxi_id";

		public static final String TRIP_SECONDS ="trip_seconds";

		public static final String TRIP_MILES ="trip_miles";

		public static final String TRIP_TOTAL ="trip_total";

		public static final String DROPOFF_AREA ="dropoff_community_area";

		public static final String PICKUP_AREA = "pickup_community_area";

		public static final String START_TIME = "trip_start_timestamp";

		public static final String END_TIME = "trip_end_timestamp";

		private BinaryHeap<Compania> company;

		private BinaryHeap<Taxi> taxis;

		public TaxiTripsManager()
		{
			company = new BinaryHeap<Compania>();
			taxis = new BinaryHeap<Taxi>();
		}

		public boolean cargarSistema(String direccionJson,RangoFechaHora r) 
		{
			JsonParser parser = new JsonParser();
			try 
			{
				/* Cargar todos los JsonObject (servicio) definidos en un JsonArray en el archivo */
				JsonArray arr= (JsonArray) parser.parse(new FileReader(direccionJson));
				/* Tratar cada JsonObject (Servicio taxi) del JsonArray */
				for (int i = 0; arr != null && i < arr.size(); i++)
				{
					JsonObject obj= (JsonObject) arr.get(i);

					String company_name = obj.get(COMPANY) != null ? obj.get(COMPANY).getAsString() : "Independent Owner";

					String taxi_id = obj.get(TAXI_ID) != null ? obj.get(TAXI_ID).getAsString() : "NaN";

					double trip_miles = obj.get(TRIP_MILES) != null ? obj.get(TRIP_MILES).getAsDouble() : -1.0;

					double trip_total = obj.get(TRIP_TOTAL) != null ? obj.get(TRIP_TOTAL).getAsDouble() : 0.0;

					String fi = obj.get(START_TIME) != null ? obj.get(START_TIME).getAsString() : "2025-09-24T00:00:00.000";

					String ff = obj.get(END_TIME) != null ? obj.get(END_TIME).getAsString() : "2025-09-24T00:00:00.000";
					
					if(r.getStartDateHour().compareTo(fi) <= 0 && r.getEndDate().compareTo(ff) >= 0)
					{
						Compania actual =  existsCompany(company_name);
						if(actual == null)
						{
							actual = company.add(new Compania(company_name));
						}
						Taxi taxi = existsTaxi(taxi_id);
						if(taxi == null)
						{
							taxi = actual.addTaxisCompania(new Taxi(taxi_id, company_name));
							taxis.add(new Taxi(taxi_id, company_name));
							actual.asignarNumDeTaxisEnRango();
						}
						taxi.asignarNumeroServiciosEnRango();
						taxi.asignarDineroEnRango(trip_total);
						taxi.asignarDistanciaEnRango(trip_miles);
						actual.asignarNumServiciosDeLaCompaniaEnRango();
					}
				}
				int totalTaxis = 0;
				int totalServices = 0;
				for(int i=1; i < company.size()+1 ; i++)
				{
					totalTaxis += company.get(i).getNumTaxisInRange();
					totalServices += company.get(i).getNumServiciosEnRango();
				}
				System.out.println(totalTaxis);
				System.out.println(totalServices);
			}
			catch (JsonIOException e1 ) 
			{
				e1.printStackTrace();
			}
			catch (JsonSyntaxException e2) 
			{
				e2.printStackTrace();
			}
			catch (FileNotFoundException e3) 
			{
				e3.printStackTrace();
			} 

			return false;
		}

		public Taxi existsTaxi(String pId)
		{
			boolean existe = false;
			Taxi miTaxi = null;
			for(int i=1; i< taxis.size()+1 && !existe; i++)
			{
				if(taxis.get(i) != null && taxis.get(i).getTaxiId().equals(pId))
				{
					miTaxi = taxis.get(i);
					existe = true;
				}
			}
			return miTaxi;
		}

		public Compania existsCompany(String pNombre)
		{
			boolean existe = false;
			Compania compania = null;
			for(int i=1; i< company.size()+1 && !existe; i++)
			{
				if(company.get(i).getNombre().equals(pNombre))
				{
					compania = company.get(i);
					existe = true;
				}
			}
			return compania;
		}

		public BinaryHeap<Taxi> taxiSort()
		{
			taxis.sort(true);
			return taxis;
		}

		public BinaryHeap<Compania>getCompaniesByNumber()
		{
			company.sort(false);
			return company;
		}

}
