package model.data_structures;



public class BinaryHeap<T extends Comparable<T>>
{

	private int size;
	private T[] array;
	private int actualSize;
	@SuppressWarnings("unchecked")
	public BinaryHeap()
	{
		size = 100;
		actualSize = 0;
		array = (T[]) new Comparable[size];
	}

	private boolean less(int i, int j)
	{  
		return array[i].compareTo(array[j]) < 0;  
	}

	private void exch(int i, int j)
	{  
		T element = array[i];
		array[i] = array[j];
		array[j] = element; 
	}



	@SuppressWarnings("unchecked")
	public T add(T element)
	{
		T ele = null;
		if(element != null)
		{
			if( size == actualSize+1)
			{
				size = (int) (size*(1.25)) ;
				T [] backUp = array;
				array = (T[]) new Comparable[size];
				for(int i=1; i < actualSize+1; i++)
				{
					array[i] = backUp[i];
				}
				array[++actualSize] = element;
				ele = array[actualSize];
				swim(actualSize);
			}
			else
			{
				array[++actualSize] = element;
				ele = array[actualSize];
				swim(actualSize);	
			}
		}
		return ele;
	}
	public T get(int position) 
	{
		return (T) array[position];
	}

	public T remove()
	{
		T elem = null;
		if(actualSize > 0)
		{	
			elem = array[1];
			exch(1, actualSize);
			sink(1);
			actualSize--;
		}
		return elem;
	}


	private void sink(T[] a, int k, int n)
	{
		while (2*k <= n)
		{
			int j = 2*k;
			if (j < n && less(a, j, j+1))
				j++;
			if (!less(a, k, j)) 
				break;
			exch(a, k, j);
			k = j;
		} 
	}

	private void exch(T[] a, int i, int j)
	{  
		T element = a[i];
		a[i] = a[j];
		a[j] = element; 
	}

	private boolean less(T[] a, int i, int j)
	{  
		return a[i].compareTo(a[j]) < 0;  
	}
	public void sort(boolean ascendente)
	{
		int N = size();
		if(ascendente)
		{		
			while (N > 1)
			{
				exch(array, 1, N--);
				sink(array, 1, N);
			}			
		}
		else
		{
			while (N > 1)
			{
				exch(array, 1, N--);
				sink(array, 1, N);
			}
			for(int i=1; i < (size()/2)+1; i++)
			{
				T temp = array[i];
				array[i] = array[size() + 1 - i];
				array[size() + 1 - i] = temp; 
			}			
		}
	}


	public void sort(T[] a)
	{
		int N = a.length;
		for (int k = N/2; k >= 1; k--)
			sink(a, k, N);
		while (N > 1)
		{
			exch(a, 1, N--);
			sink(a, 1, N);
		}
	}

	

	public boolean isEmpty()
	{
		if(actualSize == 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public int size()
	{
		return actualSize;
	}

	private void swim(int k)
	{
		while (k > 1 && less(k/2, k))
		{
			exch(k/2, k);
			k = k/2; 
		}
	}


	private void sink(int k)
	{
		while (2*k <= actualSize-1)
		{
			int j = 2*k;
			if (j < actualSize-1 && less(j, j+1))
				j++;
			if (!less(k, j)) 
				break;
			exch(k, j);
			k = j;
		} 
	}

}
