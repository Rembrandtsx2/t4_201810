package model.data_structures;

import java.util.Iterator;

public class MyArrayList<T extends Comparable<T>> implements Iterable<T>{
	  
	  private T[] array;
	  private int size;
	  private int capacity;

	  public MyArrayList() {
		  array = (T[]) new Object[10];
			size = 0;
	  }
	  
	  public MyArrayList(int initCapacity) {
	    if (initCapacity < 0) {
	      throw new IllegalArgumentException();
	    }
	    this.array = (T[]) new Object[initCapacity];
	    this.capacity = initCapacity;
	  }

	  private void validateIndex(int index) {
	    if (index < 0 || index >= size) {
	      throw new ArrayIndexOutOfBoundsException();
	    }
	  }

	  private void amortized(int newSize) {
	    if (newSize > capacity) {
	      capacity *= 2;
	      move();
	    } else if (newSize <= (capacity / 4) ) {
	      capacity /= 2;
	      move();
	    }
	  }

	  private void move() {
	    T[] newArr = (T[]) new Object[capacity];
	    System.arraycopy(array, 0, newArr, 0, size);
	    this.array = newArr;
	  }

	  private void shiftRight(int index) {
	    if (index != size - 1) {
	      System.arraycopy(array, index, array, index + 1, size - (index + 1));
	    }
	  }

	  private void shiftLeft(int index) {
	    if (index != size - 1) {
	      System.arraycopy(array, index + 1, array, index, size - (index + 1));
	    }
	  }

	  public void add(T val) {
	    amortized(size + 1);
	    array[size++] = val;
	  }

	  public void add(int index, T val) {
	    validateIndex(index);
	    amortized(size + 1);
	    shiftRight(index);
	    array[index] = val;
	    size++;
	  }

	  public void remove(int index) {
	    validateIndex(index);
	    T temp = array[index];
	    shiftLeft(index);
	    array[size--] = null;
	    amortized(size);
	  }

	  public boolean remove(T val) {
	    int index = indexOf(val);
	    if (index == -1) {
	      return false;
	    }
	    remove(index);
	    return true;
	  }

	  public int indexOf(T val) {
	    for (int i = 0; i < size; i++) {
	      T element = array[i];
	      if (element != null && element.equals(val)) {
	        return i;
	      }
	    }
	    return -1;
	  }

	  public boolean contains(T val) {
	    return indexOf(val) != -1;
	  }

	  @Override
	  public Iterator<T> iterator() {
	    return new ListIterator<T>(this);
	  }

	  public int size() {
	    return size;
	  }

	  public int capacity() {
	    return capacity;
	  }

	  public T get(int index) {
	    validateIndex(index);
	    return array[index];
	  }

	  public static <T extends Comparable<T>> void swap(MyArrayList<T> list, int i, int j) {
	    T temp = list.array[i];
	    list.array[i] = list.array[j];
	    list.array[j] = temp;
	  }

	  private static class ListIterator<T extends Comparable<T>> implements Iterator<T> {
	    private int index;
	    private int size;
	    private MyArrayList<T> list;

	    public ListIterator(MyArrayList<T> arrayList) {
	      this.list = arrayList;
	      this.index = 0;
	      this.size = arrayList.size();
	    }

	    @Override
	    public boolean hasNext() {
	      return index < size;
	    }

	    @Override
	    public T next() {
	      return list.get(index++);
	    }
	  }

	  @Override
	  public String toString() {

	    Iterator<T> it = iterator();
	    if (!it.hasNext()) {
	      return "[]";
	    }

	    StringBuilder sb = new StringBuilder();
	    sb.append('[');
	    for (;;) {
	      T e = it.next();
	      sb.append(e == this ? "(this Collection)" : e);
	      if (!it.hasNext()) {
	        return sb.append(']').toString();
	      }
	      sb.append(',').append(' ');
	    }
	  }
}
